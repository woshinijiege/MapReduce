package com.map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;



import java.io.IOException;

public class TestJob {

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        //创建一个mapreduce的工作
        Job job = Job.getInstance(conf);
        job.setJarByClass(TestJob.class);

        //map的class在哪里
        job.setMapperClass(Startmap.class);
        //map的class在哪里
        job.setReducerClass(StartReduce.class);

        //map输出的key和value的类型
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        //reduce输出key和value的类型
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        //判断output文件是否存在，如果存在则删除
        File file = new File("F:\\output");
        if (file.exists()){
            FileUtils.deleteDirectory(file);
        }
        //计算机的文件在哪里
        FileInputFormat.setInputPaths(job,new Path("f:\\b.info"));
        //计算机的结果文件在哪里
        FileOutputFormat.setOutputPath(job, new Path("f:\\output"));

        job.waitForCompletion(true);
    }
}
