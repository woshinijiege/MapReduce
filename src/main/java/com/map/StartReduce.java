package com.map;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


import java.io.IOException;

public class StartReduce extends Reducer<Text, IntWritable,Text,IntWritable> {
    /**
     *
     * @param key       所有相同的key都聚到了一起
     * @param values    所有key相同的value      的值   1 1 1 1  1 1
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        int line = 0;
        //遍历所有value的值
        for(IntWritable value : values){
            line = line + value.get();
        }
        context.write(key,new IntWritable(line));
    }
          //去重
//    @Override
//    protected void reduce(Text key, Iterable<NullWritable> values, Context context) throws IOException, InterruptedException {
//        context.write(key,NullWritable.get());
//
//    }
    //排序
//    int line = 1;
//
//    @Override
//    protected void reduce(IntWritable key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
//        for(IntWritable val : values) {
//            context.write(new IntWritable(line++), key);
//        }
//    }
    //求平均值
//      @Override
//      protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            //累计
//          int count = 0;
            //求和
//          int sum = 0;
            //求平均值
//          double avg = 0;
//          for(IntWritable value :values){
//              sum  = sum  + value.get();
//              count++;
//          }
//          avg = sum / count;
//
//          context.write(key,new DoubleWritable(avg));
//      }
            //采集电话 上行下行之和
  List<Gather> list = new ArrayList<Gather>();


    // @Override
    // protected void reduce(Text key, Iterable<Gather> values, Context context) throws IOException, InterruptedException {
    //     int pageview = 0;
    //     int downloads = 0;
    //     for (Gather g : values) {
    //         pageview = pageview + g.getPageview();
    //         downloads = downloads + g.getDownloads();
    //     }
    //     list.add(new Gather(key.toString(), pageview, downloads));
    // }


    // @Override
    // protected void cleanup(Context context) throws IOException, InterruptedException {

    //     Collections.sort(list);

    //     for (Gather gather : list) {
    //         context.write(gather, NullWritable.get());
    //     }
    // }
}
