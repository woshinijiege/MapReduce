package com.gather;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * phonenumber 手机号
 * pageview    浏览量
 * downloads   下载量
 */
public class Gather implements WritableComparable<Gather> {
    String phonenumber;
    int pageview;
    int downloads;
    int sum;
    //初始化对象  必须有
    public Gather(){}
    public Gather(String phonenumber,int pageview,int downloads,int sum){
        this.phonenumber = phonenumber;
        this.pageview = pageview;
        this.downloads = downloads;
        this.sum = sum;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public int getPageview() {
        return pageview;
    }

    public void setPageview(int pageview) {
        this.pageview = pageview;
    }

    public int getDownloads() {
        return downloads;
    }

    public void setDownloads(int downloads) {
        this.downloads = downloads;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }
    @Override
    public void write(DataOutput out) throws IOException {
        out.writeUTF(phonenumber);
        out.writeInt(pageview);
        out.writeInt(downloads);
        out.writeInt(sum);
    }
    @Override
    public void readFields(DataInput in) throws IOException {
        this.phonenumber = in.readUTF();
        this.pageview = in.readInt();
        this.downloads = in.readInt();
        this.sum = in.readInt();
    }
    @Override
    public String toString(){
        return this.phonenumber+","+this.pageview+","+this.downloads+","+this.sum;
    }
     @Override
    public int compareTo(Gather o) {
        return (o.getSum() - this.sum < 0) ?1 : -1;
    }
}

