package com.map;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
/**
 * 第一个   偏移量
 * 第二个   每一行的数据
 * 第三个   map输出key类型
 * 第四个   map输出value类型
 *
 *
 * Hadoop 实现了自己的序列化方式 Writable
 * Long         LongWritable
 * int          IntWritable
 * double       DoubleWirtable
 * string       Text
 * null         NullWritable
 *
 *
 */
public class Startmap extends Mapper<LongWritable,Text,Text, IntWritable> {
    /**
     *
     * @param key       偏移量
     * @param value     每一行数据
     * @param context   map和reduce连接的通道
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        //数据：hello,tom
        //获得每一行的数据
        String line = value.toString();
        //切单词 切分压平 ["hallo","tom"]
        String[] strArry = line.split(",");
        for(String str : strArry){
            context.write(new Text(str),new IntWritable(1));
        }
    }
      //去重
//    @Override
//    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
//        String trim = value.toString().trim();
//        context.write(new Text(trim), NullWritable.get());
//
//
//    }
//
    //排序
//    @Override
//    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
//        String line = value.toString();
//        context.write(new IntWritable(Integer.parseInt(line)),new IntWritable());
//    }
    //求平均值
//    @Override
//    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
//        String line = value.toString();
//        String[] array = line.split("\\^");
//        context.write(new Text(array[0]),new IntWritable(Integer.valueOf(array[1])));
//    }
    //采集  手机号  上行下行之和

    // @Override
    // protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
    //     String line = value.toString();

    //     String[] Array = line.split("\t");
    //     String phonenumber = Array[1];

    //     int pageview = Integer.parseInt(Array[8]);

    //     int downloads = Integer.parseInt(Array[9]);
    //     context.write(new Text(phonenumber),new Gather(phonenumber,pageview,downloads));
    // }
}
