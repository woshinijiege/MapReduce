package com.etc;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class ListingMap extends Mapper<LongWritable, Text,Text,Listing> {

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        //强制转换类型
        String line = value.toString();
        //用","切分 压平
        String[] Array = line.split(",");
        //获取单号
        String order = Array[0];
        //获取单价以及数量
        double price = Double.parseDouble(Array[3]);
        int number = Integer.parseInt(Array[4]);

        context.write(new Text(order),new Listing(order,price,number));
    }

}
