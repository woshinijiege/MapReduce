package com.etc;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.File;
import java.io.IOException;

public class ListingJob {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {


        Configuration conf = new Configuration();
        //创建一个mapreduce的工作
        Job job = Job.getInstance(conf);
        job.setJarByClass(ListingJob.class);
        //map的class在哪里
        job.setMapperClass(ListingMap.class);
        //reduce的class在哪里
        job.setReducerClass(ListingReduce.class);
        //map输出的key和value的类型
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Listing.class);
        //reduce输出的key和value的类型
        job.setOutputKeyClass(Listing.class);
        job.setOutputValueClass(NullWritable.class);

        File file = new File("F:\\output");
        if (file.exists()) {
            FileUtils.deleteDirectory(file);
        }
        //计算的文件在哪里
        FileInputFormat.setInputPaths(job, new Path("F:\\intput\\d.txt"));
        //计算的结果在哪里
        FileOutputFormat.setOutputPath(job, new Path("F:\\output"));

        job.waitForCompletion(true);
    }
}
