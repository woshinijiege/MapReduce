package com.etc;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * String   order 货品单号
 * int      price 单价
 * int      number 数量
 * int      totalprice 总价
 */
//实现序列化接口
public class Listing implements WritableComparable<Listing> {
    String order;
    double price;
    int number;
    double totalprice;
    public Listing() {}

    public Listing(String order, double price, int number) {
        this.order = order;
        this.price = price;
        this.number = number;
        this.totalprice = this.price * this.number;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public double getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(double totalprice) {
        this.totalprice = totalprice;
    }

    @Override
    public String toString() {
        return this.order+ "," + this.price + "," + this.number + "," + this.totalprice ;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeUTF(order);
        out.writeDouble(price);
        out.writeInt(number);
        out.writeDouble(totalprice);
    }
    @Override
    public void readFields(DataInput in) throws IOException {
        this.order = in.readUTF();
        this.price = in.readDouble();
        this.number = in.readInt();
        this.totalprice = in.readDouble();
    }
    @Override
    public int compareTo(Listing o) {
        return (o.getTotalprice() - this.totalprice > 0)?1 : -1;
    }
}
